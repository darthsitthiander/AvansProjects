#ifndef __DEMO_FRAME_H__
#define __DEMO_FRAME_H__

#include "Frames/Frame.h"
#include "DemoModel.h"
#include "DemoView.h"
#include "DemoController.h"
#include "SDL.h"

class DemoFrame: public Frame
{
public:
	DemoFrame(SDL_Renderer* renderer);
	~DemoFrame();

	void handleInput(SDL_Event event, const float dt);
	void handleProcess(const float dt);
	void handleDraw();
private:
	DemoModel* demoModel_			= nullptr;
	DemoView* demoView_				= nullptr;
	DemoController* demoController_	= nullptr;
};

#endif