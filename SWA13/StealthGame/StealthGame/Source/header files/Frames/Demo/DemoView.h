#ifndef __DEMO_VIEW__
#define __DEMO_VIEW__

#include "Models/ImageTexture.h"
#include "Frames/Demo/DemoController.h"

class DemoView 
{
public:
	DemoView(SDL_Renderer* renderer, DemoController* demoController);
	~DemoView();

	void handleDraw();
private:
	DemoController* demoController_	= nullptr;
	SDL_Renderer* renderer_			= nullptr;
	ImageTexture* imageTexture_		= nullptr;
};

#endif