//
//  main.cpp
//  arr3d
//
//  Created by Bob Polis on 31/08/15.
//  Copyright (c) 2015 Avans. All rights reserved.
//

#include <iostream>

#include "Array3D.h"

using namespace std;

// create an Array3D object with a sequence of integer values
template<typename T>
Array3D<T> make_box(size_t x_size, size_t y_size, size_t z_size, T val)
{
	Array3D<T> a{ x_size, y_size, z_size };
	for (size_t z{ 0 }; z < a.get_z_size(); ++z) {
		for (size_t y{ 0 }; y < a.get_y_size(); ++y) {
			for (size_t x{ 0 }; x < a.get_x_size(); ++x) {
				a.put(val++, x, y, z);
			}
		}
	}
	return a;
}

// show the contents of an Array3D object
template<typename T>
void print_box(const Array3D<T>& box)
{
	for (size_t z{ 0 }; z < box.get_z_size(); ++z) {
		for (size_t y{ 0 }; y < box.get_y_size(); ++y) {
			for (size_t x{ 0 }; x < box.get_x_size(); ++x) {
				cout << box.get(x, y, z) << ' ';
			}
		}
	}
	cout << '\n';
}

int main(int argc, const char * argv[])
{
	auto box{ make_box(2, 3, 4, "a") };
	print_box(box);

	auto doos{ box };
	print_box(doos);

	auto blikje = box;
	box.put("b", 0, 0, 0);
	print_box(box);

	cout << "box heeft dus " << (box == blikje ? "inderdaad" : "niet") << " dezelfde inhoud als blikje\n";

	std::getchar();
}