#include "Rectangle.h"

#include <ostream>
#include <iostream>

int main()
{
	Rectangle r1 { { 2, 3 },{ 4, 5 } };
	Rectangle r2 { { 6, 7 },{ 8, 9 } };
	auto r3 = r1;

	if(r1 == r2)
	{
		std::cout << "r1 is equal to r2" << std::endl;
	}
	else
	{
		std::cout << "r1 is NOT equal to r2" << std::endl;
	}
	if (r1 == r3)
	{
		std::cout << "r1 is equal to r3" << std::endl;
	}
	else
	{
		std::cout << "r1 is NOT equal to r3" << std::endl;
	}

	if (r1 > r2)
	{
		std::cout << "r1 is bigger than r2" << std::endl;
	}
	else if(r1 < r2)
	{
		std::cout << "r1 is NOT bigger than r2" << std::endl;
	}

	std::getchar();
}