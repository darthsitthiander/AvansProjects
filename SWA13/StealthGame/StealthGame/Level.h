#ifndef __LEVEL__
#define __LEVEL__

#include "Map.h"
#include "State.h"

class Level
{
public:
	Level();
	~Level();
	void setState();
	State getState();
private:
	Map map;
	State state;
};

#endif