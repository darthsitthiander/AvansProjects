#ifndef __PASSIVEITEM__
#define __PASSIVEITEM__

#include "Item.h"

class PassiveItem : Item {
public:
	PassiveItem();
	~PassiveItem();
private:
};

#endif