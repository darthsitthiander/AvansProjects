#ifndef __GAMEOVERSTATE__
#define __GAMEOVERSTATE__

#include "State.h"

class GameOverState : State {
public:
	GameOverState();
	~GameOverState();
	void Use();
private:
};

#endif