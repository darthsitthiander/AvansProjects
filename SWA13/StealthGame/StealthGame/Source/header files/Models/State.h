#ifndef __STATE__
#define __STATE__

class State {
public:
	State();
	~State();
	virtual void Use();
private:
};

#endif