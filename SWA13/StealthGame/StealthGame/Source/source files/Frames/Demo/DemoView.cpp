#include "Frames/Demo/DemoView.h"
#include "Frames/Demo/DemoModel.h"
#include "Models/ImageTexture.h"
#include <iostream>
#include "Models\Level.h"

using namespace std;

DemoView::DemoView(SDL_Renderer* renderer, DemoController* demoController)
{
	renderer_		= renderer;
	demoController_	= demoController;
}

DemoView::~DemoView()
{

}

void DemoView::handleDraw()
{
	SDL_SetRenderDrawColor(renderer_, 0xFF, 0xFF, 0xFF, 0xFF);
	SDL_RenderClear(renderer_);

	demoController_->getDemoModel()->render();

	Map* map = new Map();
	Level* level = new Level(map);

	//Render red filled quad
	SDL_Rect fillRect = { 0, SCREEN_HEIGHT / 4, SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2 };
	SDL_SetRenderDrawColor(renderer_, 0xFF, 0x00, 0x00, 0xFF);
	SDL_RenderFillRect(renderer_, &fillRect);

	SDL_RenderPresent(renderer_);
}