#ifndef __MAIN_WINDOW__
#define __MAIN_WINDOW__

#include <SDL.h>
#include "Constants/ConstantsUI.h"

class MainWindow
{
public:
	bool init();

	SDL_Renderer* getRenderer();

	void close();
private:
	SDL_Window* window_		= nullptr;
	SDL_Renderer* renderer_	= nullptr;
};

#endif