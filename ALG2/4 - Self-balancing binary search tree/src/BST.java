public class BST {
    private BSTNode root;

    public void prettyprint() {
        if(root != null) {
            root.prettyprint("→", " ");
        }
    }

    /**
     * Inserts the value into the binary search tree
     */
    public void insert(int number) {
        if(root == null) {
            root = new BSTNode(number);
        } else {
            root = root.insertAVL(number);
            if(!root.isAVLGebalanceerd()){
                root = root.balanceTree();
            }
        }
    }

    /**
     * Returns true if the number is present as a node in the tree
     */
    public boolean exists(int number) {
        throw new UnsupportedOperationException("Not implemented, yet");
    }

    /**
     * Returns the smallest value in the tree (or -1 if tree is empty)
     */
    public int min() {
        if(root == null) return -1;
        return root.min();
    }

    /**
     * Returns the largest value in the tree (or -1 if tree is empty)
     */
    public int max() {
        if(root == null) return -1;
        return root.max();
    }

    /**
     * Returns how many levels deep the deepest level in the tree is
     * (the empty tree is 0 levels deep, the tree with only one root node is 1 deep)
     * @return
     */
    public int depth() {
        if(root == null) return 0;
        return root.depth(root);
    }

    /**
     * Returns the amount of values in the tree
     * @return
     */
    public int count() {
        if(root == null) return 0;
        return root.size();
    }

    /**
     * Print all values that lie between min and max in order (inclusive)
     */
    public void printInRange(int min, int max) {
        throw new UnsupportedOperationException("Not implemented, yet");
    }

    /**
     * Delete a number from the tree (if it exists)
     */
    public void delete(int number) {
        if(root != null){
            root = root.delete(number);
            if(root == null) return;
            if(!root.isAVLGebalanceerd()){
                root = root.balanceTree();
            }
        }
    }

    public static void main(String args[]) {
        BST tree = new BST();
        /*tree.insert(50);
        //tree.prettyprint();
        tree.insert(2);
        //tree.prettyprint();
        tree.insert(7);
        //tree.prettyprint();
        tree.insert(94);
        //tree.prettyprint();
        tree.insert(24);
        //tree.prettyprint();
        tree.insert(24);
        //tree.prettyprint();
        tree.insert(71);
        //tree.prettyprint();
        tree.insert(30);
        //tree.prettyprint();
        tree.insert(49);*/
        //tree.prettyprint();
        for(int i=1; i<=10000; i++){
            tree.insert(i);
        }
        System.out.println("Count: " + tree.count()); // Should be 9
        System.out.println("Min: " + tree.min()); // Should be 2
        System.out.println("Max: " + tree.max()); // Should be 94
        System.out.println("Depth: " + tree.depth()); // Should be 7
        tree.prettyprint();

        /*tree.delete(49);
        //tree.prettyprint();
        tree.delete(51); // test for value not in tree
        //tree.prettyprint();
        tree.delete(50);
        //tree.prettyprint();
        tree.delete(2);
        //tree.prettyprint();
        tree.delete(7);
        //tree.prettyprint();
        tree.delete(94);
        //tree.prettyprint();
        tree.delete(24);
        //tree.prettyprint();
        tree.delete(24);
        //tree.prettyprint();
        tree.delete(71);
        //tree.prettyprint();
        tree.delete(30);
        //tree.prettyprint();
        tree.delete(49);
        System.out.println("Count: " + tree.count()); // Should be 0
        System.out.println("Min: " + tree.min()); // Should be -1
        System.out.println("Max: " + tree.max()); // Should be -1
        System.out.println("Depth: " + tree.depth()); // Should be 0
        tree.prettyprint(); // Prints the values in order*/
    }
}