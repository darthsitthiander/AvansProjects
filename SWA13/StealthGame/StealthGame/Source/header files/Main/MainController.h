#ifndef __MAIN_CONTROLLER__
#define __MAIN_CONTROLLER__

#include "MainWindow.h"

class MainController
{
public:
	MainController();
	~MainController();

	bool init();
	void close();
private:
	MainWindow* mainWindow_;
};

#endif