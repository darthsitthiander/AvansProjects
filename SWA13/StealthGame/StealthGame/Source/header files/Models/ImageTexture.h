#ifndef __IMAGE_TEXTURE_H__
#define __IMAGE_TEXTURE_H__

#include "SDL.h"
#include "SDL_image.h"
#include <string>

class ImageTexture
{
public:
	ImageTexture(SDL_Renderer* renderer);
	~ImageTexture();

	bool loadFromFile(std::string path);

	void free();
	void setColor(Uint8 red, Uint8 green, Uint8 blue);

	void setAlpha(Uint8 alpha);
	void setBlendMode(SDL_BlendMode blending);
	void render(int x, int y, SDL_Rect* clip = nullptr, double angle = 0.0, SDL_Point* center = nullptr, SDL_RendererFlip flip = SDL_FLIP_NONE);

	int getWidth();
	int getHeight();
private:
	SDL_Renderer* renderer_ = nullptr;
	SDL_Texture* texture_ = nullptr;

	int width_, height_;
};

#endif