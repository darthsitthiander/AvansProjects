import java.awt.*;
import javax.swing.*;

public class FractalPanel1 extends JPanel {
	
	private final int PANEL_WIDTH = 400;
	private final int PANEL_HEIGHT = 400;
	private final int TOPLEFTX = 0, TOPLEFTY = 0;
	private final int WIDTH = 300, HEIGHT = 300;
	private int currentOrder;
	private Graphics page;		// object where drawing is done
	private int counter;
	
	public FractalPanel1(int order) {
		currentOrder = order;
		setBackground(Color.black);
		setPreferredSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));
	}
	
	public void drawFractal(int depth, int x1, int y1, int width, int height) {
		
		// Draw the Sierpinski's Carpet RECURSIVELY in an area whose top left coordinate 
		// is (x1,y1) with the given width and height. Divide the area into 9 equal sized
		// squares and draw a filled rectangle in the middle square. If the order is not 1,
		// recursively draw Sierpinski Carpets of order-1 in the remaining eight squares.
		
		// COMPLETE YOUR CODE HERE:
		int widthNew, heightNew;
		double startDepthPow 	= Math.pow(3, depth);
    	
    	widthNew 				= (int) (getWidth() / startDepthPow);
		heightNew 				= (int) (getHeight() / startDepthPow);
		
		if(depth > 0){
			for(int i = 0; i < 9; i++) {
				if(i != 4){
					widthNew = width / 3;
	        		heightNew = height / 3;
	        		int xNew = x1 + ((i % 3) * widthNew);
	            	int yNew = y1 + ((i / 3) * heightNew);
	            	System.out.println("drawfractal "+ xNew + " " + yNew + " " + widthNew + " " + heightNew);
					drawFractal((depth - 1), xNew, yNew, widthNew, heightNew);
				}
			}
		} else {
			System.out.println("fill rect "+ x1 + " " + y1 + " " + widthNew + " " + heightNew);
			page.fillRect(x1, y1, width, height);
			counter++;
		}
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		page = g;
		page.setColor(Color.green);
		
		// Draw initial square region for fractal	
		//page.drawRect(TOPLEFTX, TOPLEFTY, WIDTH, HEIGHT);
		
		// Recursively draw fractal of given order in square region
		counter = 0;
		drawFractal(currentOrder, TOPLEFTX, TOPLEFTY, PANEL_WIDTH, PANEL_HEIGHT);
		System.out.println("Aantal vierkantjes: " + counter);
	}
	
	public void setOrder(int order) {
		currentOrder = order;
	}

	public int getOrder() {
		return currentOrder;
	}
}
