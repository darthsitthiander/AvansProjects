#include "Matrix.h"

#include <vector>
#include <iostream>
#include <valarray>
#include <string>
#include <math.h>
#include <cmath>
#include <iomanip>

using namespace std;

inline double roundTo3(double value)
{
	return round(value * 1000.0) / 1000.0;
}

inline double roundTo4(double value)
{
	return round(value * 10000.0) / 10000.0;
}


int main(int args[])
{
	// Week 6

	// Test
	Matrix<double, 3, 3> a1({ {
		{ 1,0,0 },
		{ 1,1,0 },
		{ 1,0,1 }
		} });
	Matrix<double, 3, 3> b1({ {
		{ 0,1,1 },
		{ 1,1,1 },
		{ 0,1,0 }
		} });
	Matrix<double, 3, 3> c1({ {
		{ 0,1,0 },
		{ 1,1,1 },
		{ 0,1,0 }
		} });

	a1.print();
	bool a1InverseExists = false;
	auto a1Res = a1.inverse(a1InverseExists);
	if (a1InverseExists)
	{
		a1Res.print();
	}
	std::cout << "Inverse of a1 " << ((a1InverseExists) ? "exists.\n" : "doesn't exist.\n");

	b1.print();
	bool b1InverseExists = false;
	auto b1Res = b1.inverse(b1InverseExists);
	if (b1InverseExists)
	{
		b1Res.print();
	}
	std::cout << "Inverse of b1 " << ((b1InverseExists) ? "exists.\n" : "doesn't exist.\n");

	c1.print();
	bool c1InverseExists = false;
	auto c1Res = c1.inverse(c1InverseExists);
	if (c1InverseExists)
	{
		c1Res.print();
	}
	std::cout << "Inverse of c1 " << ((c1InverseExists) ? "exists.\n" : "doesn't exist.\n");

	// Opgave 2
	Matrix<double, 4, 4> b({{
		{ 1,0,0,0 },
		{ 2,1,0,0 },
		{ 3,2,1,0 },
		{ 4,3,2,1 }
	}}); 
	Matrix<double, 4, 4> c({ {
		{ 1,2,3,4 },
		{ 5,6,7,8 },
		{ 9,10,11,12 },
		{ 13,14,15,16}
	} });
	Matrix<double, 4, 4> d({ {
		{ 0,3,2,1 },
		{ 1,0,3,2 },
		{ 2,1,0,3 },
		{ 3,2,1,0 }
	} });

	b.print();
	bool bInverseExists = false;
	auto bRes = b.inverse(bInverseExists);
	if (bInverseExists)
	{
		bRes.print();
	}
	std::cout << "Inverse of b " << ((bInverseExists) ? "exists.\n" : "doesn't exist.\n");

	c.print();
	bool cInverseExists = false;
	auto cRes = c.inverse(cInverseExists);
	if (cInverseExists)
	{
		cRes.print();
	}
	std::cout << "Inverse of c " << ((cInverseExists) ? "exists.\n" : "doesn't exist.\n");

	d.print();
	bool dInverseExists = false;
	auto dRes = b.inverse(dInverseExists);
	if (dInverseExists)
	{
		dRes.print();
	}
	std::cout << "Inverse of d " << ((dInverseExists) ? "exists.\n" : "doesn't exist.\n");
	
	std::getchar();
}
