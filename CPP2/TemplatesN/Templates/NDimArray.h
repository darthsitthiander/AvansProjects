//
//  NDimArray.h
//  arr3d
//
//  Created by Bob Polis on 07/10/14.
//  Copyright (c) 2014 Avans Hogeschool, 's-Hertogenbosch. All rights reserved.
//

#ifndef __arr3d__NDimArray__
#define __arr3d__NDimArray__

#include <iostream>
#include <list>
#include <type_traits>
#include <vector>

using namespace std;

template<typename T, size_t N>
class NDimArray {
public:
	// default constructor
	NDimArray()	: p{ nullptr }
	{
		cerr << "default construction\n";
	}

	// preferred constructor
	NDimArray(const std::vector<size_t> dims) : p{ nullptr }
	{
		cerr << "preferred construction\n";
		for (size_t i = 0; i < N; ++i) {
			dimensions[i] = dims.at(i);
		}
		p = new T[size()];
	}

	// copy constructor
	NDimArray(const NDimArray& other)
		: dimensions{ other.dimensions }, p{ nullptr }
	{
		cerr << "copy construction\n";
		init_storage(other);
	}

	// move constructor
	NDimArray(NDimArray&& other)
		: dimensions{ other.dimensions }, p{ other.p }
	{
		cerr << "move construction\n";
		other.p = nullptr;
	}

	// destructor (not virtual because this class is not meant to be subclassed)
	~NDimArray()
	{
		cleanup_storage();
	}

	// copy assignment
	NDimArray<T, N>& operator=(const NDimArray& other)
	{
		cerr << "copy assignment\n";
		if (this != &other) {
			cleanup_storage();
			for (size_t i = 0; i < N; ++i) {
				dimensions[i] = other.dimensions[i];
			}
			init_storage(other);
		}
		return *this;
	}

	// move assignment
	NDimArray<T, N>& operator=(NDimArray&& other)
	{
		cerr << "move assignment\n";
		if (this != &other) {
			cleanup_storage();
			for (size_t i = 0; i < N; ++i) {
				dimensions[i] = other.dimensions[i];
			}
			p = other.p;
			other.p = nullptr;
		}
		return *this;
	}

	// storing & retrieving values
	void put(const T& val, vector<size_t> coordinates) { p[index(coordinates)] = val; }
	void put(const T& val, size_t index) { p[index] = val; }
	T& get(vector<size_t> coordinates) const { return p[index(coordinates)]; }
	T& get(size_t index) const { return p[index]; }

	// number of dimensions
	size_t get_num_dimensions() const { return N; }

	// equality comparison
	bool operator==(const NDimArray& other)
	{
		if (other.dimensions != dimensions) return false;
		bool equal{ true };
		for (size_t i{ 0 }; i < size(); ++i) {
			if (other.p[i] != p[i]) {
				equal = false;
				break;
			}
		}
		return equal;
	}

	// number of elements
	size_t size() const
	{
		size_t size = 1;
		for (auto s : dimensions) size *= s;
		cout << size;
		return size;
	}

	// accessors
	std::vector<size_t> get_sizes() const { return dimensions; }
	std::vector<size_t> get_size(size_t dimension) const { return dimensions[dimension]; }

private:
	size_t dimensions[N];
	T* p { nullptr };

	// translation from (x, y, z) to flat index
	size_t index(vector<size_t> coordinates) const
	{
		size_t size = 1;
		unsigned i = 0;

		for (auto s : coordinates)
		{
			auto tempSize{ s };

			for (size_t index{ 0 }; index < i; index++) tempSize *= dimensions[index];
			size += tempSize;
		}

		return size;
	}

	vector<size_t> getCoordinates(size_t index) const
	{
		vector<size_t> coordinates;

		for (auto i{ 0 }; i < size(); i++)
		{
			auto tempSize{ i };

			tempSize /= index;

			coordinates.push_back(i + tempSize);
		}

		return coordinates;
	}

	// copy initializer, used by copy constructor and copy assignment
	void init_storage(const NDimArray& other)
	{
		size_t sz{ size() };
		p = new T[sz];
		for (size_t i{ 0 }; i < sz; ++i) {
			p[i] = other.p[i];
		}
	}

	// cleanup, used by destructor and copy assignment
	void cleanup_storage() { cleanup_storage(std::is_pointer<T>()); }

	void cleanup_storage(std::true_type)
	{
		std::cerr << "we'll delete the elements\n";
		for (size_t i = 0; i < size(); ++i)
		{
			delete p[i];
		}
		delete[] p;
		p = nullptr;
	}

	void cleanup_storage(std::false_type)
	{
		delete[] p;
		p = nullptr;
	}
};

#endif /* defined(__arr3d__NDimArray__) */