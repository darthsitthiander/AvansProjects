#ifndef __ITEM__
#define __ITEM__

#include "GameObject.h"

class Item : GameObject {
public:
	Item();
	~Item();
	void Use();
private:
};

#endif