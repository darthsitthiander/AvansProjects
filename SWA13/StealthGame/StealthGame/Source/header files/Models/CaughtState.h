#ifndef __CAUGHTOVERSTATE__
#define __CAUGHTOVERSTATE__

#include "State.h"

class CaughtState : State {
public:
	CaughtState();
	~CaughtState();
	void Use();
private:
};

#endif