//
//  Array3D.cpp
//  arr3d
//
//  Created by Bob Polis on 09/11/14.
//  Copyright (c) 2014 Avans Hogeschool, 's-Hertogenbosch. All rights reserved.
//

#include "Array3D.h"
using namespace std;

Array3D::Array3D()
	: x_size{ 0 }, y_size{ 0 }, z_size{ 0 }, p{ nullptr }
{
	cerr << "default construction\n";
}

Array3D::Array3D(size_t x_size, size_t y_size, size_t z_size)
	: x_size{ x_size }, y_size{ y_size }, z_size{ z_size }, p{ nullptr }
{
	cerr << "preferred construction\n";
	p = new int[size()];
}

Array3D::Array3D(const Array3D& other)
	: x_size{ other.x_size }, y_size{ other.y_size }, z_size{ other.z_size }, p{ nullptr }
{
	cerr << "copy construction\n";
	init_storage(other);
}

Array3D::Array3D(Array3D&& other)
	: x_size{ other.x_size }, y_size{ other.y_size }, z_size{ other.z_size }, p{ other.p }
{
	cerr << "move construction\n";
	other.p = nullptr;
}

Array3D::~Array3D()
{
	cleanup_storage();
}

Array3D& Array3D::operator=(const Array3D& other)
{
	cerr << "copy assignment\n";
	if (this != &other) {
		cleanup_storage();
		x_size = other.x_size;
		y_size = other.y_size;
		z_size = other.z_size;
		init_storage(other);
	}
	return *this;
}

Array3D& Array3D::operator=(Array3D&& other)
{
	cerr << "move assignment\n";
	if (this != &other) {
		cleanup_storage();
		x_size = other.x_size;
		y_size = other.y_size;
		z_size = other.z_size;
		p = other.p;
		other.p = nullptr;
	}
	return *this;
}

bool Array3D::operator==(const Array3D& other)
{
	if (other.x_size != x_size || other.y_size != y_size || other.z_size != z_size) return false;
	bool equal{ true };
	for (size_t i{ 0 }; i < size(); ++i) {
		if (other.p[i] != p[i]) {
			equal = false;
			break;
		}
	}
	return equal;
}

void Array3D::init_storage(const Array3D& other)
{
	size_t sz{ size() };
	p = new int[sz];
	for (size_t i{ 0 }; i < sz; ++i) {
		p[i] = other.p[i];
	}
}

void Array3D::cleanup_storage()
{
	delete[] p;
	p = nullptr;
}