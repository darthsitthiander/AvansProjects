#include "Frames/Demo/DemoFrame.h"

DemoFrame::DemoFrame(SDL_Renderer* renderer)
{
	demoModel_						= new DemoModel(renderer);
	demoController_					= new DemoController(demoModel_);
	demoView_						= new DemoView(renderer, demoController_);
}

DemoFrame::~DemoFrame()
{

}

void DemoFrame::handleInput(SDL_Event event, const float dt)	{ demoController_->handleInput(event, dt);	}
void DemoFrame::handleProcess(const float dt)	{ demoModel_->handleProcess(dt);		}
void DemoFrame::handleDraw()					{ demoView_->handleDraw();				}