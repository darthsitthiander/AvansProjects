#include "Frames/Demo/DemoModel.h"

const float DemoModel::DOT_VEL = 0.3f;

DemoModel::DemoModel(SDL_Renderer* renderer)
{
	posX_ = 0;
	posY_ = 0;

	velX_ = 0;
	velY_ = 0;

	imageTexture_ = new ImageTexture(renderer);

	imageTexture_->loadFromFile("Resources/dot.bmp");
}

DemoModel::~DemoModel()
{
	delete imageTexture_;
}

void DemoModel::handleEvent(SDL_Event& e, const float dt)
{
	SDL_Rect fillRect = { 0, SCREEN_HEIGHT / 4, SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2 };
	float tmpVelX = velX_, tmpVelY = velY_;
	bool skipDefinition = false;

	if (e.type == SDL_KEYDOWN && e.key.repeat == 0)
	{
		switch (e.key.keysym.sym)
		{
		case SDLK_UP: tmpVelY -= DOT_VEL; break;
		case SDLK_DOWN: tmpVelY += DOT_VEL; break;
		/*case SDLK_LEFT: tmpVelX -= DOT_VEL; break;
		case SDLK_RIGHT: tmpVelX += DOT_VEL; break;*/
		}
		if(collisionCheck(getBoundingBox(tmpVelX * dt, tmpVelY * dt), fillRect))
		{
			//skipDefinition = true;
		}
	}
	else if (e.type == SDL_KEYUP && e.key.repeat == 0)
	{
		switch (e.key.keysym.sym)
		{
		case SDLK_UP: tmpVelY += DOT_VEL; break;
		case SDLK_DOWN: tmpVelY -= DOT_VEL; break;
		//case SDLK_LEFT: tmpVelX += DOT_VEL; break;
		//case SDLK_RIGHT: tmpVelX -= DOT_VEL; break;
		}
	}

	if (!skipDefinition)
	{
		printf("no collision\n");
		printf("velX_: %d\n", velX_);
		printf("tmpVelX: %d\n", tmpVelX);
		printf("velY_: %d\n", velY_ * -1.0f);
		printf("tmpVelY: %d\n", tmpVelY);
		velX_ = tmpVelX;
		velY_ = tmpVelY;
		printf("setting velX_ to: %d\n", velX_);
		printf("setting velY_ to: %d\n", velY_);
	} else {
		printf("collision\n");
		printf("velX_: %d\n", velX_);
		printf("tmpVelX: %d\n", tmpVelX);
		printf("velY_: %d\n", velY_);
		printf("tmpVelY: %d\n", tmpVelY);
	}
	
}

void DemoModel::handleProcess(const float dt)
{
	posX_ += (velX_ * dt);

	if (posX_ < 0)
	{
		posX_ = 0;
	}
	else if (posX_ + DOT_WIDTH > SCREEN_WIDTH)
	{
		posX_ = SCREEN_WIDTH - DOT_WIDTH;
	}

	posY_ += (velY_ * dt);

	if (posY_ < 0)
	{
		posY_ = 0;
	}
	else if (posY_ + DOT_HEIGHT > SCREEN_HEIGHT)
	{
		posY_ = SCREEN_HEIGHT - DOT_HEIGHT;
	}
}

int DemoModel::getPosX()
{
	return posX_;
}

int DemoModel::getPosY()
{
	return posY_;
}

bool DemoModel::collisionCheck(SDL_Rect a, SDL_Rect b)
{
	//The sides of the rectangles
	int leftA, leftB;
	int rightA, rightB;
	int topA, topB;
	int bottomA, bottomB;

	//Calculate the sides of rect A
	leftA = a.x;
	rightA = a.x + a.w;
	topA = a.y;
	bottomA = a.y + a.h;

	//Calculate the sides of rect B
	leftB = b.x;
	rightB = b.x + b.w;
	topB = b.y;
	bottomB = b.y + b.h;

	//If any of the sides from A are outside of B
	if (bottomA <= topB)
	{
		return false;
	}

	if (topA >= bottomB)
	{
		return false;
	}

	if (rightA <= leftB)
	{
		return false;
	}

	if (leftA >= rightB)
	{
		return false;
	}

	//If none of the sides from A are outside B
	return true;
}

SDL_Rect DemoModel::getBoundingBox(float velX, float velY)
{
	SDL_Rect rect = {};
	rect.x = posX_ + velX;
	rect.y = posY_ + velY;
	rect.w = 20;
	rect.h = 20;
	return rect;
}

void DemoModel::render()
{
	imageTexture_->render(posX_, posY_);
}