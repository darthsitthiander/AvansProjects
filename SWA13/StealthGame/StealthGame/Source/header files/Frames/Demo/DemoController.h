#ifndef __DEMO_CONTROLLER_H__
#define __DEMO_CONTROLLER_H__

#include "Frames/Demo/DemoModel.h"

class DemoController
{
public:
	DemoController(DemoModel* demoModel);
	~DemoController();

	void handleInput(SDL_Event event, const float dt);
	DemoModel* getDemoModel();
private:
	DemoModel* demoModel_ = nullptr;
};

#endif