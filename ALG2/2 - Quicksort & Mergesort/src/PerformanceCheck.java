import java.util.Arrays;
import java.util.Random;

public class PerformanceCheck {

	public static void main(String[] args) {
		int[] arrSorted = new int[1000];
		int[] arrUnsorted = new int[1000];
		for(int i =0; i<1000; i++){
			arrSorted[i] = i;
			arrUnsorted[i] = i;
		}
		shuffleArray(arrUnsorted);
		System.out.println(Arrays.toString(arrSorted));
		System.out.println(Arrays.toString(arrUnsorted));
		System.out.println();
		
		//  quicksort
		Quicksort sorter = new Quicksort();
		int[] cpyArrUnsorted = new int[1000];
		System.arraycopy( arrUnsorted, 0, cpyArrUnsorted, 0, arrUnsorted.length );
		sorter.sort(cpyArrUnsorted, "unsorted");
		sorter.print();
		sorter.sort(arrSorted, "sorted");
		sorter.print();
		
		// quicksort3
		Quicksort3 sorter2 = new Quicksort3();
		cpyArrUnsorted = new int[1000];
		System.arraycopy( arrUnsorted, 0, cpyArrUnsorted, 0, arrUnsorted.length );
		sorter2.sort(cpyArrUnsorted, "unsorted");
		sorter2.print();
		sorter2.sort(arrSorted, "sorted");
		sorter2.print();
		
		// mergesort
		Mergesort sorter3 = new Mergesort();
		cpyArrUnsorted = new int[1000];
		System.arraycopy( arrUnsorted, 0, cpyArrUnsorted, 0, arrUnsorted.length );
		sorter3.sort(cpyArrUnsorted, "unsorted");
		sorter3.print();
		sorter3.sort(arrSorted, "sorted");
		sorter3.print();
		
		// bubblesort
		Bubblesort sorter4 = new Bubblesort();
		cpyArrUnsorted = new int[1000];
		System.arraycopy( arrUnsorted, 0, cpyArrUnsorted, 0, arrUnsorted.length );
		sorter4.sort(cpyArrUnsorted, "unsorted");
		sorter4.print();
		sorter4.sort(arrSorted, "sorted");
		sorter4.print();

	}

	// Implementing Fisher�Yates shuffle
	  static void shuffleArray(int[] ar)
	  {
	    Random rnd = new Random();
	    for (int i = ar.length - 1; i > 0; i--)
	    {
	      int index = rnd.nextInt(i + 1);
	      // Simple swap
	      int a = ar[index];
	      ar[index] = ar[i];
	      ar[i] = a;
	    }
	  }
}
