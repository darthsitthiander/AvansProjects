#ifndef __MAIN_LOOP__
#define __MAIN_LOOP__

#include "Frames/Frame.h"

class MainLoop
{
public:
	MainLoop();
	~MainLoop();
	void run();
	void switchFrame(Frame* frame, bool remainBackgroundFrame);
private:
	Frame* activeFrame_;
	Frame* backgroundFrame_;
};

#endif