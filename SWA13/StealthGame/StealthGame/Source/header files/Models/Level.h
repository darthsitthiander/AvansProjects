#ifndef __LEVEL__
#define __LEVEL__

#include "Map.h"
#include "State.h"

class Level
{
public:
	Level();
	Level(Map* map);
	~Level();
	void setState();
	State getState();
	Map* getMap();
private:
	Map* map_ = nullptr;
	State state;
};

#endif