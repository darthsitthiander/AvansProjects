#ifndef __TILE__
#define __TILE__

#include <vector>
#include <list>
#include <map>
#include "Direction.h"

class GameObject
{
public:
	GameObject();
	~GameObject();
	void Use();
private:
	char color[6];
	std::list<std::vector<int>> shape;
	bool isSolid;
	std::map<Direction, int> m;
};

#endif
