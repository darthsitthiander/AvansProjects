public class BSTNode {
    private int number;

    private BSTNode left;
    private BSTNode right;

    public BSTNode(int number) {
        this.number = number;
    }

    /**
     * Add a number in the subtree of this node
     */
    public BSTNode insertAVL(int number) {
        if(number < this.number) {
            // Smaller value, insert it into the left subtree
            if(this.left == null) {
                this.left = new BSTNode(number);
            } else {
                this.left.insertAVL(number);
            }
        } else {
            // Larger or equal value, insert it in the right subtree
            if(this.right == null) {
                this.right = new BSTNode(number);
            } else {
                this.right.insertAVL(number);
            }
        }
        return this;
    }

    public void prettyprint(String firstPrefix, String prefix) {
        System.out.println(firstPrefix + number + " (" + ((left == null) ? "0" : depth(left)) + "|" + ((right == null) ? "0" : depth(right)) + ")");

        if (right == null) {
            System.out.println(prefix + "├── .");
        } else {
            right.prettyprint(prefix + "├── ", prefix + "|   ");
        }

        if (left == null) {
            System.out.println(prefix + "└── .");
        } else {
            left.prettyprint(prefix + "└── ", prefix + "    ");
        }
    }

    private int countChildren(BSTNode node) {
        int size = 1;

        if(node.left != null) size += countChildren(node.left);
        if(node.right != null) size += countChildren(node.right);

        return size;
    }

    private int countDirectChildren(BSTNode node){
        int count = 0;
        if(node.left != null) count += 1;
        if(node.right != null) count += 1;
        return count;
    }

    public int size() {
        return this.countChildren(this);
    }

    public int depth(BSTNode node) {
        int depthLeft = 0;
        int depthRight = 0;

        if(node.left != null) depthLeft += depth(node.left);
        if(node.right != null) depthRight += depth(node.right);

        return 1 + Math.max(depthLeft, depthRight);
    }

    public int min() {
        return recursive_min(this);
    }

    private int recursive_min(BSTNode node) {
        if(node.left != null){
            return node.left.number;
        }
        return number;
    }

    public int max() {
        return recursive_max(this).number;
    }

    private BSTNode recursive_max(BSTNode node) {
        if(node.right != null){
            return node.right;
        }
        return node;
    }

    public BSTNode delete(int number) {
        return recursive_delete(this, number);
    }

    private BSTNode recursive_delete(BSTNode node, int number){
        if(node == null) return null;
        if(node.number > number) {
            node.left = recursive_delete(node.left, number);
        }
        else if(node.number < number) {
            node.right = recursive_delete(node.right, number);
        }
        else {
            switch(countDirectChildren(node)){
                case 0:
                    return null;
                case 1:
                    return (node.left == null) ? node.right : node.left;
                case 2:
                    return recursive_max(node.left);
            }
        }
        return node;
    }

    public BSTNode rotateLeft(){
        BSTNode root = this;
        BSTNode pivot = (root.right != null) ? root.right : null;

        if(pivot != null){
            BSTNode tempNode = (pivot.left != null) ? pivot.left : null;
            root.right = (tempNode != null) ? tempNode : null;
            pivot.left = root;
        }

        return pivot;
    }

    public BSTNode rotateRight(){
        BSTNode root = this;
        BSTNode pivot = (root.left != null) ? root.left : null;

        if(pivot != null){
            BSTNode tempNode = (pivot.right != null) ? pivot.right : null;
            root.left = (tempNode != null) ? tempNode : null;
            pivot.right = root;
        }

        return pivot;
    }

    public boolean isAVLGebalanceerd(){
        BSTNode nodeLeft;
        BSTNode nodeRight;
        int depthLeft = 0;
        int deptRight = 0;
        boolean isLeftBalanced = true;
        boolean isRightBalanced = true;

        nodeLeft = (this.left != null) ? this.left : null;
        nodeRight = (this.right != null) ? this.right : null;
        if(nodeLeft != null){
            depthLeft = depth(nodeLeft);
            isLeftBalanced = nodeLeft.isAVLGebalanceerd();
        }
        if(nodeRight != null){
            deptRight = depth(nodeRight);
            isLeftBalanced = nodeRight.isAVLGebalanceerd();
        }

        return isLeftBalanced && isRightBalanced && !(Math.abs(depthLeft - deptRight) > 1);
    }

    public BSTNode balanceTree() {
        BSTNode node = this;
        return balance(node);
    }

    private BSTNode balance(BSTNode node){
        BSTNode nodeLeft;
        BSTNode nodeRight;
        int depthLeft = 0;
        int deptRight = 0;

        nodeLeft = (node.left != null) ? node.left : null;
        nodeRight = (node.right != null) ? node.right : null;
        if(nodeLeft != null){
            depthLeft = depth(nodeLeft);
            node.left = nodeLeft.balanceTree();
        }
        if(nodeRight != null){
            deptRight = depth(nodeRight);
            node.right = nodeRight.balanceTree();
        }

        if((Math.abs(depthLeft - deptRight) > 1)){
            node = fixNode(this);
        }

        return node;
    }

    private String getWeight(BSTNode node){
        int[] weights = new int[2];

        if(node.left != null) weights[0] = depth(node.left);
        if(node.right != null) weights[1] = depth(node.right);

        return (weights[0] > weights[1]) ? "left" : "right";
    }

    // LL
    private BSTNode restructureLeftLeft(BSTNode root){
        // rotate root to right
        root = root.rotateRight();
        return root;
    }

    // LR
    private BSTNode restructureLeftRight(BSTNode root){
        BSTNode pivot = root.left;
        // rotate pivot to left
        root.left = pivot.rotateLeft();
        // rotate root to right
        root = root.rotateRight();
        return root;
    }

    // RL
    private BSTNode restructureRightLeft(BSTNode root){
        BSTNode pivot = root.right;
        // rotate pivot to right
        root.right = pivot.rotateRight();
        // rotate root to left
        root = root.rotateLeft();
        return root;
    }

    // RR
    private BSTNode restructureRightRight(BSTNode root){
        // rotate root to left
        root = root.rotateLeft();
        return root;
    }

    public BSTNode fixNode(BSTNode node){
        if(getWeight(node).equals("left") && getWeight(node.left).equals("left")){
            // LL
            node = restructureLeftLeft(node);
        } else if(getWeight(node).equals("left") && getWeight(node.left).equals("right")){
            // LR
            node = restructureLeftRight(node);
        } else if(getWeight(node).equals("right") && getWeight(node.right).equals("left")){
            // RL
            node = restructureRightLeft(node);
        } else if(getWeight(node).equals("right") && getWeight(node.right).equals("right")){
            // RR
            node = restructureRightRight(node);
        }
        return node;
    }
}