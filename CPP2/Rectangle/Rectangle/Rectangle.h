#pragma once

struct Point {
	float x;
	float y;

	bool operator==(const Point& other) const
	{
		return x == other.x && y == other.y;
	}
};

struct Size {
	float width;
	float height;

	bool operator>(const Size& other) const
	{
		return width * height > other.width * other.height;
	}

	bool operator<(const Size& other) const
	{
		return width * height < other.width * other.height;
	}
};

class Rectangle
{
public:
	Rectangle(){}
	Rectangle::Rectangle(Point origin, Size size) : origin_(origin), size_(size) {}

	bool operator==(const Rectangle& other) const;
	bool operator>(const Rectangle& other) const;
	bool operator<(const Rectangle& other) const;
private:
	Point origin_{ 0, 0 };
	Size size_{ 0, 0 };
};