#include <string>
#include <iostream>
#include <math.h>
#include <iomanip>
#include <sstream>

std::string decimalToString(double decimal, int precision)
{
	std::stringstream stream;
	stream << std::fixed << std::setprecision(precision) << decimal;
	return stream.str();
}

std::string from1To2(double x1, double y1, double x2, double y2)
{
	double a, b;

	b = (x2 - x1 != 0) ? (y2 - y1) / (x2 - x1) : 0;
	a = y1 - (b * x1);

	return "y = " + decimalToString(a, 2) + " + " + decimalToString(b, 2) + "x";
}

std::string from1To3(double x1, double y1, double x2, double y2)
{
	auto g = x2 - x1;
	auto h = y2 - y1;

	return "(x / y) = (" + decimalToString(x1, 2) + " / " + decimalToString(y1, 2) + ") + ?(" + decimalToString(g, 2) + " / " + decimalToString(h, 2) + ")";
}

std::string from2To1(double a, double b)
{
	auto x1 = 0;
	auto x2 = 1;
	auto y1 = a + b * x1;
	auto y2 = a + b * x2;

	return "P(" + decimalToString(x1, 2) + "/" + decimalToString(y1, 2) + "), Q(" + decimalToString(x2, 2) + "/" + decimalToString(y2, 2) + ")";
}

std::string from2To3(double a, double b)
{
	auto x1 = 0;
	auto x2 = 1;

	return "(x / y) = (0 / " + decimalToString(a, 2) + ") + ?(1 / " + decimalToString(b, 2) + ")" ;
}

std::string from3To1(double e, double f, double g, double h)
{
	return "P(" + decimalToString(e, 2) + "/" + decimalToString(f, 2) + "), Q(" + decimalToString(e+g, 2) + "/" + decimalToString(f+h, 2) + ")";
}

std::string from3To2(double e, double f, double g, double h)
{
	double a, b;

	b = (g != 0) ? (h / g) : 0;
	a = f - b * e;

	return "y = " + decimalToString(a, 2) + " + " + decimalToString(b, 2) + "x";
}

int main(int argc, const char* argv[])
{
	std::cout << from1To2(6, 3, 2, 8) << std::endl;
	std::cout << from1To3(6, 3, 2, 8) << std::endl << std::endl;

	std::cout << from2To1(6.34, 6.34) << std::endl;
	std::cout << from2To3(6.34, 6.34) << std::endl << std::endl;

	std::cout << from3To1(3, 3, 1, 1) << std::endl;
	std::cout << from3To2(3,3,1,1) << std::endl << std::endl;

	std::cout << from1To2(0.34, 0.62, 0.34, 1.62) << std::endl;
	std::cout << from1To3(0.34,0.62,0.34,1.62) << std::endl << std::endl;

	std::cout << from2To1(64, 32) << std::endl;
	std::cout << from2To3(64, 32) << std::endl << std::endl;

	std::cout << from3To1(0.04, 3.34, 0, 1.21) << std::endl;
	std::cout << from3To2(0.04,3.34,0,1.21) << std::endl << std::endl;

	std::cout << from1To2(234,445,612,823) << std::endl;
	std::cout << from1To3(234, 445, 612, 823) << std::endl << std::endl;

	std::cout << from2To1(0, 1) << std::endl;
	std::cout << from2To3(0, 1) << std::endl << std::endl;

	std::cout << from3To1(4, 2, 0, 0) << std::endl;
	std::cout << from3To2(4,2,0,0) << std::endl << std::endl;

	std::getchar();
}
