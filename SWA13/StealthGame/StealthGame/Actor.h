#ifndef __ACTOR__
#define __ACTOR__

#include "Item.h"
#include "State.h"

class Actor {
public:
	Actor();
	~Actor();
	Item inventory[15];
	State getState();
	void setSate();
	void addItem();
private:
	int rangeofSight;
	float speed;
};

#endif