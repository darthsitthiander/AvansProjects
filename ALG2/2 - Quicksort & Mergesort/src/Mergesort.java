import java.util.Arrays;

public class Mergesort {
  private int[] numbers;
  private int[] helper;

  private int number;
  public int cComparisons = 0;
  public int cSwaps = 0;
  public long executionTime = 0;
private String order;

  public void sort(int[] values, String order) {
	  this.order = order;
	final long startTime = System.nanoTime();
    this.numbers = values;
    number = values.length;
    this.helper = new int[number];
    mergesort(0, number - 1);
    executionTime = (System.nanoTime() - startTime) / 1000000;
  }
  
  public void print(){
  	  System.out.println("Mergesort " + this.order + ": aantal vergelijkingen (" + this.cComparisons + "), aantal swaps (" + this.cSwaps + "), uitvoertijd (" + this.executionTime  + "ms)");
  	  //System.out.println(Arrays.toString(this.numbers));
  	  System.out.println();
    }

  private void mergesort(int low, int high) {
    // check if low is smaller then high, if not then the array is sorted
    if (low < high) {
      // Get the index of the element which is in the middle
      int middle = low + (high - low) / 2;
      // Sort the left side of the array
      mergesort(low, middle);
      // Sort the right side of the array
      mergesort(middle + 1, high);
      // Combine them both
      merge(low, middle, high);
    }
  }

  private void merge(int low, int middle, int high) {

    // Copy both parts into the helper array
    for (int i = low; i <= high; i++) {
      helper[i] = numbers[i];
    }

    int i = low;
    int j = middle + 1;
    int k = low;
    // Copy the smallest values from either the left or the right side back
    // to the original array
    while (i <= middle && j <= high) {
    	  this.cComparisons++;
    	  
	      if (helper[i] <= helper[j]) {
	        numbers[k] = helper[i];
	        i++;
	      } else {
	        numbers[k] = helper[j];
	        j++;
	        this.cSwaps++;
	      }
	      k++;
    }
    // Copy the rest of the left side of the array into the target array
    while (i <= middle) {
      numbers[k] = helper[i];
      k++;
      i++;
    }

  }
}