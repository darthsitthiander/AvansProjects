import java.util.Arrays;

public class Quicksort3 {
    static final int CUTOFF = 10;
    
    private int[] numbers;
    public int cComparisons = 0;
    public int cSwaps = 0;
    public long executionTime = 0;

	private String order;

    public void sort (int[]a, String order) {
  	  this.order = order;
    	final long startTime = System.nanoTime();
    	this.numbers = a;
        quickSort3(0, a.length-1);
        executionTime = (System.nanoTime() - startTime) / 1000000;
    }
    
    public void print(){
  	  System.out.println("Quicksort3 " + this.order + ": aantal vergelijkingen (" + this.cComparisons + "), aantal swaps (" + this.cSwaps + "), uitvoertijd (" + this.executionTime  + "ms)");
  	  //System.out.println(Arrays.toString(this.numbers));
  	  System.out.println();
    }

    private void quickSort3 (int left, int right) {
		while (true) {
		    if (right-left <= CUTOFF) {            // small subarray
				insertionSort(left, right);
				return;
		    }
		    int pivot = median3(numbers, left, right); // does a lot
		    int i = left, j = right-1;            // subtle
		    while (true) {
		    	this.cComparisons++;
				while (numbers[++i] < pivot) ;
				while (numbers[--j] > pivot) ;
				if (i >= j)
				    break;
				else {
				    // swapElements (a, i, j);
				    int tmp = numbers[i];
				    numbers[i] = numbers[j];
				    numbers[j] = tmp;
				    this.cSwaps++;
				}
		    }
		    // swapElements (a, i, right-1);
		    int tmp = numbers[i];
		    numbers[i] = numbers[right-1];
		    numbers[right-1] = tmp;
		    this.cSwaps++;
		    // quickSort3 (a, left, i-1);
		    // quickSort3 (a, i+1, right);
		    if ((i-1)-left < right-(i+1)) {
				quickSort3 (left, i-1);
				left = i+1;
		    } else {
				quickSort3 (i+1, right);
				right = i-1;
		    }
		}
    }

    private int median3 (int[]a, int left, int right) {
        int center = (left+right) / 2;
        if (a[center] < a[left]) {
            // swapElements (a, left, center);
		    int tmp = a[center];
		    a[center] = a[left];
		    a[left] = tmp;
		}
        if (a[right] < a[left]) {
            // swapElements (a, left, right);
		    int tmp = a[left];
		    a[left] = a[right];
		    a[right] = tmp;
		}
        if (a[right] < a[center]) {
            // swapElements (a, center, right);
		    int tmp = a[center];
		    a[center] = a[right];
		    a[right] = tmp;
		}
        // swapElements (a, center, right-1); // move pivot out
	    int tmp = a[center];
	    a[center] = a[right-1];
	    a[right-1] = tmp;
        return a[right-1];          
    }

    //     private final static void swapElements (long[] a, int i, int j) {
    //     long tmp = a[i];
    //     a[i] = a[j];
    //     a[j] = tmp;
    // }
        
    private void insertionSort (int left, int right) {
        int j;
        for (int i=left+1; i<=right; i++) {
            int tmp = numbers[i];
            for (j=i; j>left && tmp<numbers[j-1]; j--)
            	numbers[j] = numbers[j-1];
            numbers[j] = tmp;
        }
    }
}
// Local Variables:
// c-basic-offset: 4
// compile-command: "javac -Xlint:unchecked QuickSort3.java && java QuickSort3"
// End:

        