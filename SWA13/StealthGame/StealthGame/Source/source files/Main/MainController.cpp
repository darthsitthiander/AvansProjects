#include "Main/MainController.h"
#include "Main/MainLoop.h"
#include "Frames/Demo/DemoFrame.h"

#include <iostream>

MainController::MainController()
{
	mainWindow_					= new MainWindow;
}

MainController::~MainController()
{
	delete mainWindow_;
}

bool MainController::init()
{
	bool success				= mainWindow_->init();

	DemoFrame* demoFrame		= new DemoFrame(mainWindow_->getRenderer());

	if (success)
	{
		MainLoop mainLoop;
		mainLoop.switchFrame(demoFrame, false);
		mainLoop.run();
		close();
	}

	return success;
}

void MainController::close()
{
	mainWindow_->close();

	mainWindow_					= nullptr;
}