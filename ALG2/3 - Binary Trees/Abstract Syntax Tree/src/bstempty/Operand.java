package bstempty;

public class Operand extends Expression {

    private char operand;
    private Expression left, right;

    public Operand(char operand, Expression left, Expression right) {
        this.operand = operand;
        this.left = left;
        this.right = right;
    }

    @Override
    public int evaluate() {
        switch(operand){
            case '*':
                return left.evaluate() * right.evaluate();
            case '/':
                return left.evaluate() / right.evaluate();
            case '+':
                return left.evaluate() + right.evaluate();
            case '-':
                return left.evaluate() - right.evaluate();
            default: return 0;
        }
    }

    public String toString(){
        return "(" + left.toString() + operand + right.toString() + ")";
    }
}