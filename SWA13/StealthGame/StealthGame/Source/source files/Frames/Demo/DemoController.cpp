#include "Frames/Demo/DemoController.h"
#include "Frames/Demo/DemoModel.h"

DemoController::DemoController(DemoModel* demoModel)
{
	demoModel_ = demoModel;
}

DemoController::~DemoController()
{

}

void DemoController::handleInput(SDL_Event event, const float dt)
{
	demoModel_->handleEvent(event, dt);
}

DemoModel* DemoController::getDemoModel() 
{
	return demoModel_;
}