#ifndef __MAP__
#define __MAP__

#include <list>
#include "Tile.h"

class Map
{
public:
	Map();
	~Map();
	void render();
private:
	std::list<Tile> tiles;
};

#endif