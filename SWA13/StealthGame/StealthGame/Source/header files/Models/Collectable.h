#ifndef __COLLECTABLE__
#define __COLLECTABLE__

#include "Item.h"

class Collectable : Item {
public:
	Collectable();
	~Collectable();
private:
};

#endif