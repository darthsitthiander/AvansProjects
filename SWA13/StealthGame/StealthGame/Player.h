#ifndef __PLAYER__
#define __PLAYER__

#include "Actor.h"

class Player : Actor {
public:
	Player();
	~Player();
	void PerformAction();
private:
	int tries;
	int triesLimit;
	Item* selectedItem;
	Item passiveItems[5];
	Item collectableItems[50];
};

#endif