#ifndef __PLAYINGSTATE__
#define __PLAYINGSTATE__

#include "State.h"

class PlayingState : State {
public:
	PlayingState();
	~PlayingState();
	void Use();
private:
};

#endif