#include "SDL.h"
#include <iostream>
#include "Main/MainController.h"

int main(int argc, char* args[])
{
	bool success						= true;
	MainController* mainController		= new MainController();

	if (mainController->init() == false)
	{
		printf("ERROR! StealthGame Could not be initialized!");

		success = false;
	}

	delete mainController;

	return success;
}