#ifndef __FRAME_H__
#define __FRAME_H__

#include "SDL.h"

class Frame
{
public:
	virtual void handleInput(SDL_Event event, const float dt) = 0;
	virtual void handleProcess(const float dt) = 0;
	virtual void handleDraw() = 0;
};

#endif