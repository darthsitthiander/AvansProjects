import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.*;

public class Recursie {
	
	static int width = 450;
	static int height = 450;
	
	public static void main(String[] args) {
		String masterstring = "Wij zijn koolfreters!";
		
		// Pyramide probleem
		System.out.println(iteratief_driehoeksgetal(6)); // tweedsnellst
		System.out.println(recursief_driehoeksgetal(6)); // StackoverflowError
		System.out.println(driehoeksgetal(6)); // snellste want berekeing
		
		// Fibonacci
		// Uitleg: onthoud een cijfer en voeg deze toe aan een string die aan het einde wordt gereturned. Daarna roep de methode recursief op met het onthouden cijfer tot het gespecifeerde maximalgetal is bereikt
		String reeks = "";
		for(int i=0; i<9; i++){
			reeks += fibonacci_recursive(i) + " ";
		}
		System.out.println(reeks); // recursief is langzaam en duur lang tot de stackoverflow error optreed
		System.out.println(fibonacci_iteratief(9)); // snel maar krijg OutOfMemoryError 
		// recursie is vaak lanzamer dan iteratie omdat iedere method call in een stack moet worden gecopieerd om het pad weer terug te kunnen volgen
		
		
		// neiaardmo gnirtS
		System.out.println(reverse(masterstring));
		if(isPalindrome("MAOAM")){
			System.out.println("It's a palindrome.");
		} else {
			System.out.println("It's NOT a palindrome.");
		}
		
		// Carpet of Sierpinsky
		// Mijn laptop kan 5 niveaus en 32768 vierkantjes aan
	}

	private static boolean isPalindrome(String string) {
		if(string.length() > 1){
			/*if(string.toLowerCase().substring(0, 1).equals(string.toLowerCase().substring(string.length()-1, string.length()))){
				String remainingString = string.substring(1,string.length()-1);
				if(remainingString.length() == 1 || isPalindrome(remainingString)){
					return true;
				}
			}*/
			return string.equals(reverse(string));
		}
		return false;
	}

	private static String fibonacci_iteratief(int n) {
		String reeks = "";
		int[] a = new int[n];
		a[0] = 0;
		reeks += a[0] + " ";
		if(n>0){
			a[1] = 1;
			reeks += a[1] + " ";
		    for(int i=2; i<n; i++) {
		    	a[i] = (a[i-1] + a[i-2]);
		    	reeks += a[i] + " ";
		    }
		}
	    return reeks;
	}

	private static int fibonacci_recursive(int n) {
		if (n <= 1) return n;
	    else return fibonacci_recursive(n-1) + fibonacci_recursive(n-2);
		
	}

	private static int driehoeksgetal(int i) {
		return (int) ((0.5 * i) * (i + 1));
	}

	private static int recursief_driehoeksgetal(int i) {
		if(i>0){
			return i + recursief_driehoeksgetal(i - 1);
		}
		return 0;
	}

	private static int iteratief_driehoeksgetal(int i) {
		int tmp = 0;
		for(int j=0; j<=i; j++){
			tmp += j;
		}
		return tmp;
	}

	private static String reverse(String masterstring) {
		if(masterstring.length()>0){
			return masterstring.substring(masterstring.length() - 1) + reverse(masterstring.substring(0, masterstring.length()-1));
		}
		return ""; // base case
	}
}
