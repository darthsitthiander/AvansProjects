#pragma once
#include <cstdio>
#include <iostream>
#include <vector>
#define _USE_MATH_DEFINES
#include <math.h>

template<typename T, size_t R, size_t C>
class Matrix;

template<typename T, size_t R>
class Vector : public Matrix<T,R,1>
{
private:
	T a[R];
public:
	explicit Vector(const std::vector<std::vector<T>>& lis)
	{
		for (size_t i = 0; i < R; ++i)
			try
		{
			a[i] = T(lis.at(0).at(i));
		}
		catch (...)
		{
			throw std::range_error("matrix access row out of range");
		}
	}

	// Subscript operators
	T& operator() (unsigned row)
	{
		if (row >= R)
			throw std::range_error("Matrix subscript out of bounds");
		return a[row];
	}

	T operator() (unsigned row) const
	{
		if (row >= R)
			throw std::range_error("const Matrix subscript out of bounds");
		return a[row];
	}

	template<size_t S>
	static double inproduct(Vector<T, R> a, Vector<T, S> b)
	{
		if (R != S) return double();

		return a(0) * b(0) + a(1) * b(1) + a(2) * b(2);
	}
	template<size_t S>
	static Vector uitproduct(Vector<T, R> a, Vector<T, S> b)
	{
		if (R != 3 || S != 3) return Vector<T, R>({{}});

		Vector<T, R> v({ { {a(1) * b(2) - b(1) * a(2) },{ b(0) * a(2) - a(0) * b(2) },{ a(0) * b(1) - b(0) * a(1) } } });
		return v;
	}

	void print()
	{
		for (auto r = 0; r < R; r++)
		{
			std::cout << "|";
			std::cout << a[r];
			printf("|\n");
		}
		printf("\n");
	}
};

template<typename T, size_t R, size_t C>
class Matrix {
public:
	Matrix() : _rows(R), _columns(C)
	{
		if (_rows == 0 || _columns == 0)
			throw std::range_error("Matrix constructor has 0 size");
		_elements = new T[R * C]{0};
	}
	explicit Matrix(const std::vector<std::vector<T>>& lis) : Matrix()
	{
		auto i = 0;
		for (auto it = lis.begin(); it != lis.end(); ++it)
		{
		auto j = 0;
			for (auto itInner = it->begin(); itInner != it->end(); ++itInner)
			{
				try
				{
					_elements[_columns*i + j] = T(*itInner);
				}
				catch (...)
				{
					throw std::range_error("matrix access row out of range");
				}
				j++;
			}
			i++;
		}
	}

	// Destructor
	~Matrix()
	{
		delete[] _elements;
	}
	// Copy constructor
	Matrix(const Matrix<T, R, C>& original) : Matrix()
	{
		_rows = original._rows;
		_columns = original._columns;
		//_elements = original._elements;*/
		for (auto i = 0; i < R; ++i)
			for (auto j = 0; j < C; ++j)
				_elements[C*i + j] = T(original(i,j));
		
	}

	//Matrix& operator= (const Matrix& m);   // Assignment operator
										   
	// Subscript operators
	T& operator() (unsigned row, unsigned col)
	{
		range_check(row, col);

		return _elements[_columns*row + col];
	}
	
	T operator() (unsigned row, unsigned col) const
	{
		range_check(row, col);

		return _elements[_columns*row + col];
	}

	void range_check(unsigned row, unsigned col) const
	{
		if (row >= _rows || col >= _columns)
			throw std::range_error("const Matrix subscript out of bounds");
	}

	template<size_t Y, size_t Z>
	Matrix<T, R, Z> operator*(const Matrix<T, Y, Z>& other)
	{
		if (Y != C)
			throw std::range_error("const Matrix subscript out of bounds");

		Matrix<T, R, Z> c;

		for (auto i = 0; i < R; i++)
		{
			for (auto j = 0; j < Z; j++)
			{
				c._elements[Z*i + j] = 0;
				for (auto k = 0; k < C; k++)
				{
					c._elements[Z*i+j] += T(at(i, k) * other(k, j));
				}
			}
		}
		return c;
	}

	void modifyRow(T r1, T r2, T v)
	{
		T plusValue;
		for (int k = 0; k < C; k++)
		{
			plusValue = v * at(r2, k);
			at(r1, k) = at(r1, k) + plusValue;
		}
	}

	void divideRow(T r, T v)
	{
		if (round(v) == 0) return;

		for (int k = 0; k < C; k++)
		{
			if(at(r, k) != 0) 
				at(r, k) = at(r, k) / v;
		}
	}

	void swapRows(T r1, T r2)
	{
		Matrix<T, R, C> mR(*this);

		for (auto i = 0; i < C; i++)
		{
			at(r1, i) = mR(r2, i);
			at(r2, i) = mR(r1, i);
		}
	}

	Matrix<T, R, C> inverse(bool& b_inverse_exists)
	{
		if (R != C) return Matrix<T, R, C>();

		int biggest;
		T value;

		Matrix<T, R, C> identity_matrix;
		for (int i = 0; i < R; i++)
		{
			identity_matrix(i, i) = 1;
		}

		// Vegen (3)
		compare(0, &biggest);

		// Vegen (4)
		if (biggest != 0)
		{
			swapRows(0, biggest);
			identity_matrix.swapRows(0, biggest);
		}

		// Vegen (5)
		divideByValue(0, &value, &identity_matrix);

		// Vegen (6)
		veeg(0, &identity_matrix);

		// Vegen (7)
		compare(1, &biggest);

		// Vegen (8)
		if (biggest != 1)
		{
			swapRows(1, biggest);
			identity_matrix.swapRows(1, biggest);
		}
		divideByValue(1, &value, &identity_matrix);

		// Vegen (9)
		veeg(1, &identity_matrix);

		// Vegen (10)
		compare(2, &biggest);

		// Vegen (11)
		if (biggest != 2)
		{
			swapRows(2, biggest);
			identity_matrix.swapRows(2, biggest);
		}
		divideByValue(2, &value, &identity_matrix);

		// Vegen (12)
		veeg(2, &identity_matrix);

		// Vegen (13)
		value = at(3, 3);
		if (value != 0)
		{
			divideRow(3, value);
			identity_matrix.divideRow(3, value);

			// Vegen (14)
			veeg(3, &identity_matrix);

			b_inverse_exists = true;
			return identity_matrix;
		}

		return Matrix<T, R, C>();
	}

	T roundTo3(T value)
	{
		return round(value * 1000.0) / 1000.0;
	}

	void roundMatrix()
	{
		for (auto i = 0; i < R; i++)
		{
			for (auto j = 0; j < C; j++)
			{
				if(abs(roundTo3(at(i, j))) == 0)
					at(i, j) = 0;
			}
		}
	}

	void compare(int start, int* biggest)
	{
		*biggest = -1;

		for (int j = start; j < R; j++)
		{
			auto compareValue = (*biggest > -1) ? abs(at(*biggest, 0)) : -1;
			auto value = abs(at(j, 0));
			if (value > compareValue)
			{
				*biggest = j;
			}
		}
	}

	void divideByValue(int val, T* value, Matrix<T, R, C>* identity_matrix)
	{
		*value = at(val, val);
		divideRow(val, *value);
		identity_matrix->divideRow(val, *value);

		roundMatrix();
		identity_matrix->roundMatrix();
	}

	void veeg(int column, Matrix<T, R, C>* identity_matrix)
	{
		T result;
		for (int l = 0; l < R; l++)
		{
			if (l == column) continue;

			auto one = at(l, column);
			auto two = at(column, column);
			if (two != 0)
			{
				result = one / two;
				if (result != 0)
				{
					modifyRow(l, column, -result);
					identity_matrix->modifyRow(l, column, -result);
				}
			}
		}
		roundMatrix();
		identity_matrix->roundMatrix();
	}

	static Matrix<T, 2, 1> scale(double velocity)
	{
		return Matrix<T, 2, 1>({ { { static_cast<T>(1 + velocity / 200) },{ static_cast<T>(1 - velocity / 400) } } });
	}

	void print()
	{
		for (auto r = 0; r < R; r++)
		{
			std::cout << "|";
			for (auto c = 0; c < C; c++)
			{
				std::cout << at(r,c) << (c < C - 1 ? "\t" : "");
			}
			printf("|\n");
		}
		printf("\n");
	}

	T& at(size_t row, size_t col)
	{
		return _elements[_columns*row + col];
	}

	int toRadians(int rate);

	T* _elements;
	unsigned _rows, _columns;
};

template<typename T, size_t R, size_t C>
class Matrix3D : public Matrix<T,R,C>
{
public:
	Matrix3D() : Matrix() {}

	explicit Matrix3D(const std::vector<std::vector<T>>& lis) : Matrix(lis) {}

	Matrix3D(const Matrix3D<T, R, C>& original) : Matrix(original) {}
	Matrix3D(const Matrix<T, R, C>& original) : Matrix(original){}

	template<size_t Y, size_t Z>
	Matrix3D<T, R, Z> operator*(const Matrix3D<T, Y, Z>& other)
	{
		if (Y != C)
			throw std::range_error("const Matrix subscript out of bounds");

		Matrix3D<T, R, Z> c;

		for (auto i = 0; i < R; i++)
		{
			for (auto j = 0; j < Z; j++)
			{
				c._elements[Z*i + j] = 0;
				for (auto k = 0; k < C; k++)
				{
					c._elements[Z*i + j] += T(at(i, k) * other(k, j));
				}
			}
		}
		return c;
	}

	Matrix3D<double, 4, 4> rotate(double alpha, double x, double y, double z)
	{
		double t1 = (std::atan2(z, x));
		double t2 = (std::atan2(y, sqrt(x*x + z*z)));
		auto r1 = RotateOverY(t1);
		auto r2 = RotateOverZ(t2);
		auto rA = RotateOverX(toRadians(alpha));
		auto r3 = RotateOverZ(-t2);
		auto r4 = RotateOverY(-t1);

		return r4 * r3 * rA * r2 * r1;
	}

	Matrix3D<double, 4, 4> rotate(double alpha, double x, double y, double z, double Mx, double My, double Mz)
	{
		double t1 = (std::atan2(z, x));
		double t2 = (std::atan2(y, sqrt(x*x + z*z)));
		auto r1 = RotateOverY(t1);
		auto r2 = RotateOverZ(t2);
		auto rA = RotateOverX(toRadians(alpha));
		auto r3 = RotateOverZ(-t2);
		auto r4 = RotateOverY(-t1);
		auto mt1 = Translate(-Mx, -My, -Mz);
		auto mt2 = Translate(Mx, My, Mz);

		return mt2 * r4 * r3 * rA * r2 * r1 * mt1;
	}

	static Matrix3D<T, 3, 3> Scale(T x, T y, T z)
	{
		return Matrix3D<T, 3, 3>({{ {x,0,0},{0,y,0},{0,0,z} }});
	}

	static Matrix3D<double, 4, 4> Translate(double x, double y, double z)
	{
		return Matrix3D<double, 4, 4>({
		{
		{ 1,0,0,x },
		{ 0,1,0,y },
		{ 0,0,1,z },
		{ 0,0,0,1 }
		} });
	}
//protected:
	// https://en.wikipedia.org/wiki/Rotation_matrix

	// radians alpha
	static Matrix3D<double, 4, 4> RotateOverX(double alpha)
	{
		double s = roundTo4(sin((alpha)));
		double c = roundTo4(cos((alpha)));
		return Matrix3D<double, 4, 4>({
			{
				{1,	0,		0,		0},
				{0,	c,		-s,		0},
				{0,	s,		c,		0},
				{0,	0,		0,		1}
			} });
	}

	static long double toRadians(double alpha)
	{
		return round(alpha * M_PI / 180);
	}

	static long double toDegrees(double alpha)
	{
		return round(alpha * 180 / M_PI);
	}

	static Matrix3D<double, 4, 4> RotateOverY(double alpha)
	{
		double s = roundTo4(sin((alpha)));
		double c = roundTo4(cos((alpha)));
		return Matrix3D<double, 4, 4>({
			{
				{c,		0,	-s,		0},
				{0,		1,	0,		0},
				{s,		0,	c,		0},
				{0,		0,	0,		1}
			} });
	}
	static Matrix3D<double, 4, 4> RotateOverZ(double alpha)
	{
		double s = roundTo4(sin((alpha)));
		double c = roundTo4(cos((alpha)));
		return Matrix3D<double, 4, 4>({
			{
				{c,		-s,		0,	0},
				{s,		c,		0,	0},
				{0,		0,		1,	0},
				{0,		0,		0,	1}
			} });
	}
};