public class BSTNode {
    public int number;

    public BSTNode left;
    public BSTNode right;

    public BSTNode(int number) {
        this.number = number;
    }

    /**
     * Add a number in the subtree of this node
     */
    public void insert(int number) {
        if(number < this.number) {
            // Smaller value, insert it into the left subtree
            if(this.left == null) {
                this.left = new BSTNode(number);
            } else {
                this.left.insert(number);
            }
        } else {
            // Larger value, insert it in the right subtree
            if(this.right == null) {
                this.right = new BSTNode(number);
            } else {
                this.right.insert(number);
            }
        }
    }

    public void traverse(){
        if(this.left != null) this.left.traverse();
        System.out.println(this.number);
        if(this.right != null) this.right.traverse();
    }

    private int countChildren(BSTNode node) {
        int size = 1;

        if(node.left != null) size += countChildren(node.left);
        if(node.right != null) size += countChildren(node.right);

        return size;
    }

    private int countDirectChildren(BSTNode node){
        int count = 0;
        if(node.left != null) count += 1;
        if(node.right != null) count += 1;
        return count;
    }

    public int size() {
        return this.countChildren(this);
    }

    public int depth(BSTNode node) {
        int depthLeft = 0;
        int depthRight = 0;

        if(node.left != null) depthLeft += depth(node.left);
        if(node.right != null) depthRight += depth(node.right);

        return 1 + Math.max(depthLeft, depthRight);
    }

    public int min() {
        return recursive_min(this);
    }

    private int recursive_min(BSTNode node) {
        if(node.left != null){
            return node.left.number;
        }
        return number;
    }

    public int max() {
        return recursive_max(this).number;
    }

    private BSTNode recursive_max(BSTNode node) {
        if(node.right != null){
            return node.right;
        }
        return node;
    }

    //public BSTNode delete(int number) {
        //return recursive_delete(this, number);
    //}

    public void delete(int number, BSTNode parent){

        if(parent == null) return;

        if(this.number > number) {
            if(left != null) left.delete(number, this);
        }
        else if(this.number < number) {
            if(right != null) right.delete(number, this);
        }
        else {
            /*if(this.left != null){
                //returnNode = this.left;
                this.left.right = this.right;
                //this.left = returnNode;
            }
            if(this.right != null){
                returnNode = this.right;
                returnNode.left = this.left;
                this.right = returnNode;
            }*/
            if(left != null && right != null){
                this.number = right.min();
                right.delete(this.number, this);
            }
            else if (parent.left == this) parent.left = (left != null) ? left : right;
            else if (parent.right == this) parent.right = (right != null) ? right : left;
        }
    }
}