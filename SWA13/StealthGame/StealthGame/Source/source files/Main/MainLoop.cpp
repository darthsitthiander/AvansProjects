#include "Main/MainLoop.h"
#include "SDL.h"
#include <iostream>

MainLoop::MainLoop()
{

}

MainLoop::~MainLoop()
{

}

void MainLoop::switchFrame(Frame* frame, bool remainBackgroundFrame)
{
	if (remainBackgroundFrame)
	{
		backgroundFrame_ = activeFrame_;
	}
	else
	{
		backgroundFrame_ = nullptr;
	}

	activeFrame_ = frame;
}

void MainLoop::run()
{
	bool running		= true;

	int fps				= 60;

	float dt			= 1000 / fps;

	float currentTime	= 0.0f;
	float newTime		= 0.0f;
	float frameTime		= 0.0f;
	float accumulator	= 0.0f;
	float maxFrameTime	= 1000.0f;

	SDL_Event event;

	while (running)
	{
		newTime			= SDL_GetTicks();
		frameTime		= newTime - currentTime;

		if (frameTime > maxFrameTime)
		{
			frameTime - maxFrameTime;
		}

		currentTime	= newTime;

		while (SDL_PollEvent(&event) != 0)
		{
			//if (inputHandler.handleEvent(event))
			//{

			//}
			//else
			if (event.type == SDL_QUIT)
			{
				running = false;
			}
			else
			{
				//ik gebruik even de input op deze manier, om de gameloop te testen. graag weghalen nadat de input goed is geimplementeerd.
				activeFrame_->handleInput(event, dt);
			}
			//else if...
		}

		//dit is de ideale gameloop, werkt nog niet. voor nu het delen van deltaTime door dt werkt ook voldoende
		//voor het programeren van de modellen zal het geen verschil moeten maken.
		accumulator += frameTime;

		while (accumulator >= dt)
		{
			activeFrame_->handleProcess(dt);
			accumulator -= dt;
		}

		activeFrame_->handleProcess(accumulator/dt);

		//activeFrame_->handleProcess(frameTime/dt);

		//activeFrame_->handleInput();
		activeFrame_->handleDraw();

		SDL_Delay(5); //is voor het testen op lagere fps dan je daadwerkelijke systeem aankan, in productie zal dit uitstaan / lopen via VSYNC (zie mainWindow.cpp hiervoor)
	}
}