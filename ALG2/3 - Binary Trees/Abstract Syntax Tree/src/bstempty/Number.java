package bstempty;

public class Number extends Expression {

    private int number;
    public Number(int number)
    {
        this.number = number;
    }

    @Override
    public int evaluate() {
        return this.number;
    }

    public String toString(){
        return String.valueOf(this.number);
    }
}