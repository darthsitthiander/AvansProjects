//
//  main.cpp
//  arr3d
//
//  Created by Bob Polis on 31/08/15.
//  Copyright (c) 2015 Avans. All rights reserved.
//

#include <iostream>

#include "NDimArray.h"
#include <vector>

using namespace std;

// create an NDimArray object with a sequence of integer values
template<typename T, size_t N>
NDimArray<T, N> fill_box(NDimArray<T, N>& box, T val)
{
	for (auto i{ 0 }; i < box.size(); i++)
	{
		box.put(val++, i);
	}

	return box;
}

// show the contents of an NDimArray object
template<typename T, size_t N>
void print_box(const NDimArray<T, N>& box)
{
	for (auto i{ 0 }; i < box.size(); i++)
	{		
		cout << box.get(i);
	}

	cout << '\n';
}

int main(int argc, const char * argv[])
{
	NDimArray<float, 4> box{ { 2, 3, 4, 5 } };
	fill_box(box, 42.7f);
	print_box(box);
	
	auto doos{ box };
	print_box(doos);

	auto blikje = box;
	fill_box(blikje, 61.8f);
	print_box(blikje);

	std::cout << "box heeft dus " << (box == blikje ? "inderdaad" : "niet") << " dezelfde inhoud als blikje\n";
	
	std::getchar();
}