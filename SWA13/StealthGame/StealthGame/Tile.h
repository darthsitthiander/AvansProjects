#ifndef __TILE__
#define __TILE__

class Tile
{
public:
	Tile();
	~Tile();
private:
	char color[6];
	int width;
	int height;
};

#endif