#include "Models/ImageTexture.h"

ImageTexture::ImageTexture(SDL_Renderer* renderer)
{
	renderer_ = renderer;
}

ImageTexture::~ImageTexture()
{

}

bool ImageTexture::loadFromFile(std::string path)
{
	free();

	SDL_Texture* newTexture = NULL;

	SDL_Surface* loadedSurface = IMG_Load(path.c_str());
	if (loadedSurface == NULL)
	{
		printf("Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError());
	}
	else
	{
		SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0xFF, 0xFF, 0xFF));

		newTexture = SDL_CreateTextureFromSurface(renderer_, loadedSurface);

		if (newTexture == NULL)
		{
			printf("Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
		}
		else
		{
			width_ = loadedSurface->w;
			height_ = loadedSurface->h;
		}

		SDL_FreeSurface(loadedSurface);
	}

	texture_ = newTexture;
	
	return texture_ != NULL;
}

void ImageTexture::free()
{
	if (texture_ != NULL)
	{
		SDL_DestroyTexture(texture_);
		texture_ = NULL;
		width_ = 0;
		height_ = 0;
	}
}

void ImageTexture::setColor(Uint8 red, Uint8 green, Uint8 blue)
{
	SDL_SetTextureColorMod(texture_, red, green, blue);
}

void ImageTexture::setBlendMode(SDL_BlendMode blending)
{
	SDL_SetTextureBlendMode(texture_, blending);
}

void ImageTexture::setAlpha(Uint8 alpha)
{
	SDL_SetTextureAlphaMod(texture_, alpha);
}

void ImageTexture::render(int x, int y, SDL_Rect* clip, double angle, SDL_Point* center, SDL_RendererFlip flip)
{
	SDL_Rect renderQuad = {x, y, width_, height_};

	if (clip != NULL)
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}

	SDL_RenderCopyEx(renderer_, texture_, clip, &renderQuad, angle, center, flip);
}

int ImageTexture::getWidth()
{
	return width_;
}

int ImageTexture::getHeight()
{
	return height_;
}