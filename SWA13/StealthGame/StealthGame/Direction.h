#ifndef __DIRECTION__
#define __DIRECTION__

enum Direction { NORTH, EAST, SOUTH, WEST, UP, DOWN };

#endif
