#ifndef __DEMO_MODEL__
#define __DEMO_MODEL__

#include "SDL.h"
#include "Constants/ConstantsUI.h"
#include "Models/ImageTexture.h"

class DemoModel
{
public:
	DemoModel(SDL_Renderer* renderer);
	~DemoModel();

	void handleProcess(const float dt);

	static const int DOT_WIDTH = 20;
	static const int DOT_HEIGHT = 20;

	static const float DOT_VEL;

	void handleEvent(SDL_Event& e, const float dt);

	bool collisionCheck(SDL_Rect a, SDL_Rect b);
	SDL_Rect getBoundingBox(float velX, float velY);
	void render();

	int getPosX();
	int getPosY();
private:
	float posX_, posY_,
		velX_, velY_;
	ImageTexture* imageTexture_ = nullptr;
};

#endif