#include "SDL.h"
#include <iostream>

#include "Main/MainWindow.h"

bool MainWindow::init()
{
	bool success = true;

	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
		success = false;
	}
	else
	{
		if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1"))
		{
			printf("Warning: Linear texture filtering not enabled!");
		}

		window_ = SDL_CreateWindow(GAME_NAME, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		
		if (window_ == NULL)
		{
			printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
			success = false;
		}
		else
		{
			renderer_ = SDL_CreateRenderer(window_, -1, SDL_RENDERER_ACCELERATED); // | SDL_RENDERER_PRESENTVSYNC); //VSYNC uitgezetom te testen op lagere en hogere fps, in productie zullen we dit wel gaan gebruiken

			if (renderer_ == NULL)
			{
				printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			}
			else
			{
				SDL_SetRenderDrawColor(renderer_, 0xFF, 0xFF, 0xFF, 0xFF);
			}
		}
	}

	return success;
}

SDL_Renderer* MainWindow::getRenderer()
{
	return renderer_;
}

void MainWindow::close()
{
	SDL_DestroyRenderer(renderer_);
	SDL_DestroyWindow(window_);

	renderer_			= nullptr;
	window_				= nullptr;

	SDL_Quit;
}