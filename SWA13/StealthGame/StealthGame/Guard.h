#ifndef __GUARD__
#define __GUARD__

#include "Actor.h"

class Guard : Actor {
public:
	Guard();
	~Guard();
private:
};

#endif