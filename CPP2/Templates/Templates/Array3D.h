//
//  Array3D.h
//  arr3d
//
//  Created by Bob Polis on 07/10/14.
//  Copyright (c) 2014 Avans Hogeschool, 's-Hertogenbosch. All rights reserved.
//

#ifndef __arr3d__Array3D__
#define __arr3d__Array3D__

#include <iostream>

template<class T>
class Array3D {
public:
	Array3D(); // default constructor
	Array3D(size_t x_size, size_t y_size, size_t z_size); // preferred constructor
	Array3D(const Array3D<T>& other); // copy constructor
	Array3D(Array3D<T>&& other); // move constructor

	~Array3D(); // destructor (not virtual because this class is not meant to be subclassed)

	Array3D<T>& operator=(const Array3D<T>& other); // copy assignment
	Array3D<T>& operator=(Array3D<T>&& other); // move assignment

	// storing & retrieving values
	void put(const T val, size_t x, size_t y, size_t z) { p[index(x, y, z)] = val; }
	T get(size_t x, size_t y, size_t z) const { return p[index(x, y, z)]; }

	bool operator==(const Array3D<T>& other); // equality comparison

	// number of elements
	size_t size() const { return x_size * y_size * z_size; }

	// accessors
	size_t get_x_size() const { return x_size; }
	size_t get_y_size() const { return y_size; }
	size_t get_z_size() const { return z_size; }

private:
	size_t x_size, y_size, z_size;

	T* p;

	// translation from (x, y, z) to flat index
	size_t index(size_t x, size_t y, size_t z) const { return y_size * x_size * z + x_size * y + x; }

	// copy initializer, used by copy constructor and copy assignment
	void init_storage(const Array3D<T>& other);

	// cleanup, used by destructor and copy assignment
	void cleanup_storage();
};

//--- Array3D.cpp

template<typename T>
Array3D<T>::Array3D()
	: x_size{ 0 }, y_size{ 0 }, z_size{ 0 }, p{ nullptr }
{
	cerr << "default construction\n";
}
template<typename T>
Array3D<T>::Array3D(size_t x_size, size_t y_size, size_t z_size)
	: x_size{ x_size }, y_size{ y_size }, z_size{ z_size }, p{ nullptr }
{
	cerr << "preferred construction\n";
	p = new T[size()];
}

template<typename T>
Array3D<T>::Array3D(const Array3D<T>& other)
	: x_size{ other.x_size }, y_size{ other.y_size }, z_size{ other.z_size }, p{ nullptr }
{
	cerr << "copy construction\n";
	init_storage(other);
}

template<typename T>
Array3D<T>::Array3D(Array3D<T>&& other)
	: x_size{ other.x_size }, y_size{ other.y_size }, z_size{ other.z_size }, p{ other.p }
{
	cerr << "move construction\n";
	other.p = nullptr;
}

template<typename T>
Array3D<T>::~Array3D()
{
	cleanup_storage();
}

template<typename T>
Array3D<T>& Array3D<T>::operator=(const Array3D<T>& other)
{
	cerr << "copy assignment\n";
	if (this != &other) {
		cleanup_storage();
		x_size = other.x_size;
		y_size = other.y_size;
		z_size = other.z_size;
		init_storage(other);
	}
	return *this;
}

template<typename T>
Array3D<T>& Array3D<T>::operator=(Array3D<T>&& other)
{
	cerr << "move assignment\n";
	if (this != &other) {
		cleanup_storage();
		x_size = other.x_size;
		y_size = other.y_size;
		z_size = other.z_size;
		p = other.p;
		other.p = nullptr;
	}
	return *this;
}

template<typename T>
bool Array3D<T>::operator==(const Array3D<T>& other)
{
	if (other.x_size != x_size || other.y_size != y_size || other.z_size != z_size) return false;
	bool equal{ true };
	for (size_t i{ 0 }; i < size(); ++i) {
		if (other.p[i] != p[i]) {
			equal = false;
			break;
		}
	}
	return equal;
}

template<typename T>
void Array3D<T>::init_storage(const Array3D<T>& other)
{
	size_t sz{ size() };
	p = new T[sz];
	for (size_t i{ 0 }; i < sz; ++i) {
		p[i] = other.p[i];
	}

}

template<typename T>
void Array3D<T>::cleanup_storage()
{
	delete[] p;
	p = nullptr;
}

#endif /* defined(__arr3d__Array3D__) */