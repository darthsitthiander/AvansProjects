#include "Rectangle.h"

bool Rectangle::operator==(const Rectangle& other) const
{
	return origin_ == other.origin_;
}

bool Rectangle::operator>(const Rectangle& other) const
{
	return size_ > other.size_;
}

bool Rectangle::operator<(const Rectangle& other) const
{
	return size_ < other.size_;
}