import java.util.Arrays;

public class Bubblesort {
	  private int[] numbers;

	  private int number;
	  public int cComparisons = 0;
	  public int cSwaps = 0;
	  public long executionTime = 0;

	private String order;

	  public void sort(int[] values, String order) {
		  this.order = order;
		final long startTime = System.nanoTime();
	    this.numbers = values;
	    number = values.length;
	    bubblesort(this.numbers, number - 1);
	    executionTime = (System.nanoTime() - startTime) / 1000000;
	  }
	  
	  public void print(){
	  	  System.out.println("Bubblesort " + this.order + ": aantal vergelijkingen (" + this.cComparisons + "), aantal swaps (" + this.cSwaps + "), uitvoertijd (" + this.executionTime  + "ms)");
	  	  //System.out.println(Arrays.toString(this.numbers));
	  	  System.out.println();
	    }

	  public void bubblesort( int a[], int n){
		  int i, j,t=0;
		  for(i = 0; i < n; i++){
			  for(j = 1; j < (n-i); j++){
				  if(a[j-1] > a[j]){
					  t = a[j-1];
					  a[j-1]=a[j];
					  a[j]=t;
					  this.cSwaps++;
				  }
				  this.cComparisons++;
			  }
		  }
	  }
	}